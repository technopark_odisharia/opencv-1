import numpy as np
import cv2
import argparse

CASCADE = "./haar-hand.xml"


def create_blank(width, height, rgb_color=(0, 0, 0)):
    """Create new image(numpy array) filled with certain color in RGB"""
    # Create black blank image
    image = np.zeros((height, width, 3), np.uint8)

    # Since OpenCV uses BGR, convert the color first
    color = tuple(reversed(rgb_color))
    # Fill image with color
    image[:] = color

    return image

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--in_video', type=str, help='input video path', default='video.avi')
    parser.add_argument('--in_cascade', type=str, help='input cascade path', default=CASCADE)

    args = parser.parse_args()

    cap = cv2.VideoCapture(args.in_video)

    # Create new blank 600x600 white image
    width, height = 600, 600

    white = (255, 255, 255)
    imageg = create_blank(width, height, rgb_color=white)


    prev = {'x': 0, 'y': 0}
    new = {'x': 0, 'y': 0}
    threshold_min = 30
    threshold_max = 150

    while(cap.isOpened()):
        ret, frame = cap.read()
        image = frame

    	#threshold_min = len(frame) / 10
    	#threshold_max = len(frame[0]) 	

	haar_cascade = cv2.CascadeClassifier(args.in_cascade)

        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

    	detects = haar_cascade.detectMultiScale(frame, 1.17, minNeighbors=7, minSize=(100,100))
	
        if len(detects):	
        	max_width = max(max([(w, h) for x, y, w, h in detects]))
        	detects = [x for x in detects if x[2] == max_width or x[3] == max_width]	

	for (x,y,w,h) in detects:
		if prev['x'] == 0 and prev['y'] == 0:
			new['x'] = x + w/2
			new['y'] = y + h/2
	#	elif threshold_min < np.sqrt( (prev['x'] - (x + w/2))**2 + (prev['y'] - (y + h/2))**2 ) < threshold_max:
	#		new['x'] = x + w/2
	#		new['y'] = y + h/2

		elif (threshold_min) < np.sqrt( (prev['x'] - (x + w/2))**2 + (prev['y'] - (y + h/2))**2 ) < (threshold_max):
			new['x'] = x + w/2
			new['y'] = y + h/2

	
	if (prev['x'] != 0 or prev['y'] != 0):
       		cv2.rectangle(image,(x,y),(x+w,y+h),(255,0,0),2)
		print(prev['x'], prev['y'], new['x'], new['y'])
		cv2.line(imageg, (prev['x'], prev['y']), (new['x'], new['y']), (0,0,0))

	prev['x'] = new['x']
	prev['y'] = new['y'] 


        cv2.imshow('frame',gray)
        cv2.imshow('frame1',image)
	cv2.imshow('frame2',imageg)
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break

    cap.release()
    cv2.destroyAllWindows()

if __name__ == "__main__":
    main()

